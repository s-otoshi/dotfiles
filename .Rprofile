######################################################################
##
## Description:
##
##   This is my R profile. Nothing more, nothing less.
##
## Setting Items:
##
##   * Enable Japanese label
##   * R CRAN mirror
##
######################################################################

## Enable Japanese label ---------------------------------------------
##
## Configuration for plotting without garbled Japanese in R.
##
## (As far as I looked) the earliest source is written by なかま
## in 2009-06-21:
##
## from: http://hosho.ees.hokudai.ac.jp/~kubo/ce/RgJpFont.html#toc8
##
## for explanation of each function and codes:
##
## from: https://gist.github.com/amano41/4173965

## Add font setting hook to graphics device
setHook(packageEvent("grDevices", "onLoad"),
        function(...){

          ## windows
          if(.Platform$OS.type == "windows")
            grDevices::windowsFonts(sans ="MS Gothic",
                                    serif="MS Mincho",
                                    mono ="FixedFont")

          ## Mac OS
          if(capabilities("aqua"))
            grDevices::quartzFonts(
              sans =grDevices::quartzFont(
                c("Hiragino Kaku Gothic Pro W3",
                  "Hiragino Kaku Gothic Pro W6",
                  "Hiragino Kaku Gothic Pro W3",
                  "Hiragino Kaku Gothic Pro W6")),
              serif=grDevices::quartzFont(
                c("Hiragino Mincho Pro W3",
                  "Hiragino Mincho Pro W6",
                  "Hiragino Mincho Pro W3",
                  "Hiragino Mincho Pro W6")))

          ## Linux
          if(capabilities("X11"))
            grDevices::X11.options(
              fonts=c("-kochi-gothic-%s-%s-*-*-%d-*-*-*-*-*-*-*",
                      "-adobe-symbol-medium-r-*-*-%d-*-*-*-*-*-*-*"))

          ## pdf
          grDevices::pdf.options(family="Japan1GothicBBB")

          ## postscript
          grDevices::ps.options(family="Japan1GothicBBB")
        }
)

## create Japanese environment
attach(NULL, name = "JapanEnv")

## create hook function for setting the font
assign("familyset_hook",
       function() {
         winfontdevs=c("windows","win.metafile",
                       "png","bmp","jpeg","tiff")
         macfontdevs=c("quartz","quartz_off_screen")
         devname=strsplit(names(dev.cur()),":")[[1L]][1]

         ## windows
         if ((.Platform$OS.type == "windows") &&
               (devname %in% winfontdevs))
           par(family="sans")

         ## Mac OS
         if (capabilities("aqua") &&
               devname %in% macfontdevs)
           par(family="sans")
       },
       pos="JapanEnv")

## add fonts hook function
setHook("plot.new", get("familyset_hook", pos="JapanEnv"))
setHook("persp", get("familyset_hook", pos="JapanEnv"))

## R CRAN mirror -----------------------------------------------------
##
## from: https://stackoverflow.com/questions/11488174/how-to-select-a-cran-mirror-in-r

local({
  r <- getOption("repos")
  r["CRAN"] <- "https://cloud.r-project.org"
  options(repos=r)
})

## Install Packages --------------------------------------------------
##
## My use packages if not installed, install it.

local({
  use_packages <- c("tidyverse", "hash")
  new_packages <- use_packages[!(use_packages %in% utils::installed.packages()[,"Package"])]
  if (length(new_packages)){
    utils::install.packages(new_packages)
  }
  invisible(lapply(use_packages, require, character.only = TRUE))
})

## Set Color Universal Variables -------------------------------------
##
## Color design for diverse color vision. (for ggplot2 hex pallet)
##
## ref: http://jfly.iam.u-tokyo.ac.jp/colorset/

## color universal pallet for display
dcup <- hash(
  "red" = "#FF4B00",
  "yellow" = "#FFF100",
  "green" = "#03AF7A",
  "blue" = "#005AFF",
  "sky" = "#4DC4FF",
  "pink" = "#FF8082",
  "orange" = "#F6AA00",
  "purple" = "#990099",
  "brown" = "#804000"
)

## color universal pallet for print
pcup <- hash(
  "red" = "#FF4000",
  "yellow" = "#FFFF00",
  "green" = "#40FF00",
  "blue" = "#0000FF",
  "sky" = "#73FFFF",
  "pink" = "#FF7300",
  "orange" = "#FF8C00",
  "purple" = "#B300FF",
  "brown" = "#730000"
)

# Local Variables:
# mode: R
# coding: utf-8-unix
# End:
