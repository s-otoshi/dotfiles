#!/usr/bin/env perl

# Generate PDF directly using lualatex
$pdf_mode = 1;

# Define command to compile with synctex-support and nonstopmode
$pdflatex = 'lualatex -synctex=1 --interaction=nonstopmode %O %S';

# Use bibtex if a .bib file exists
$bibtex_use = 1;

# Use utf-8 bibtex
$bibtex = 'upbibtex';

# Define max repeat compile
$max_repeat = 10;

# Local Variables:
# coding: utf-8-unix
# End:
