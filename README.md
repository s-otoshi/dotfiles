# dotfiles

## About

My dotfiles. Nothing more, nothing less.

## Clone to Local

	git clone git@bitbucket.org:s-otoshi/dotfiles.git ~/.dotfiles
