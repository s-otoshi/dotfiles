######################################################################
#
# Description:
#
#   This is makefile for setting dotfile.
#
# Reference:
#
# - babarot
#     https://github.com/b4b4r07/dotfiles/blob/master/Makefile
#
######################################################################

DOTPATH    := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
CANDIDATES := $(wildcard .??*) texmf
EXCLUSIONS := .git
DOTFILES := $(filter-out $(EXCLUSIONS), $(CANDIDATES))

list:
	@$(foreach val, $(DOTFILES), /bin/ls -dF $(val);)

install:
	@$(foreach val, $(DOTFILES), ln -sfnv $(abspath $(val)) $(HOME)/$(val);)

clean:
	@echo "Remove dotfiles from the home directory..."
	@-$(foreach val, $(DOTFILES), rm -vrf $(HOME)/$(val);)
