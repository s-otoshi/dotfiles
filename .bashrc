#!/usr/bin/env bash

### Description:

#   This is My bashrc. Nothing more, nothing less.

# インタラクティブシェルか確認
case $- in
    *i*) ;;
      *) return;;
esac

### History

# ヒストリーに重複するものとスペースで始まるコマンドを保存しない
HISTCONTROL=ignoreboth

# ヒストリーを上書きせず、追加書き込みする
shopt -s histappend

# ヒストリーサイズ
HISTSIZE=10240
HISTFILESIZE=10240

# Windowのサイズを更新した場合にLINEとCOLUMNの値を変更する
shopt -s checkwinsize

### Prompt

source ~/.bash_prompt

# TERM Title
export TERM_TITLE
TERM_TITLE="bash $USER@$HOSTNAME"

### LESS

# lessでzipなどのバイナリファイルを見やすくする
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# lessのソースハイライト
export LESS='-R'
if [ -e /usr/share/source-highlight/src-hilite-lesspipe.sh ]; then
    export LESSOPEN='| /usr/share/source-highlight/src-hilite-lesspipe.sh %s'
fi

### COMPLETION

# bash-completionを読み込み
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

### Aliases
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

### Path

# gibo
if [ -e $HOME/.gibo/gibo ]; then
    export PATH="$HOME/.gibo:$PATH"
    source $HOME/.gibo/shell-completions/gibo-completion.bash
fi

# rbenv
if [ -e $HOME/.rbenv/bin/rbenv ]; then
    export PATH="$HOME/.rbenv/bin:$PATH"
    eval "$(rbenv init -)"
    source $HOME/.rbenv/completions/rbenv.bash
    # plugins
    export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
    export PATH="$HOME/.rbenv/plugins/rbenv-update/bin/:$PATH"
fi

# pyenv
if [ -e $HOME/.pyenv/bin/pyenv ]; then
    export PATH="$HOME/.pyenv/bin:$PATH"
    eval "$(pyenv init -)"
    source $HOME/.pyenv/completions/pyenv.bash
fi

# pipenv
export PIPENV_VENV_IN_PROJECT=1

# ccache

if which ccache &> /dev/null; then
    export USE_CCACHE=1
    export CCACHE_DIR=/var/ccache-cache
    export CCACHE_LOGFILE=/var/log/ccache.log
fi

### Display

if [[ $(uname -a) =~ Microsoft ]]; then
    export DISPLAY=:0
fi

### .bashrc ends here
# Local Variables:
# outline-regexp: "###?"
# coding: utf-8-unix
# End:
