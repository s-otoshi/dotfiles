#!/usr/bin/env bash

### Description:

# This is my bash alias.

### ls
if [ -x /usr/bin/dircolors ]; then
  test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
  alias ls='ls --color=auto --ignore={NTUSER*,ntuser*}'
  alias dir='dir --color=auto'
  alias vdir='vdir --color=auto'
  alias grep='grep --color=auto'
  alias fgrep='fgrep --color=auto'
  alias egrep='egrep --color=auto'
fi

alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'

### Alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

### trash put
if which trash-put &> /dev/null; then
    alias rm=trash-put
fi

if which rmtrash &> /dev/null; then
    alias rm=rmtrash
fi

### full upgrade command in ubuntu
if [ -e /etc/lsb-release ]; then
    alias full_update="sudo apt update && sudo apt upgrade -y"
fi

### emacsclient
if which emacsclientw &> /dev/null; then
    alias e="emacsclientw --no-wait"
    export EDITOR=emacsclientw
fi

### .bash_aliases ends here
# Local Variables:
# outline-regexp: "###?"
# coding: utf-8-unix
# End:
